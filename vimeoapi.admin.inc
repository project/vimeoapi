<?php

/**
 * Form: settings
 */

function vimeoapi_settings_form($form, &$form_state) {

  // @TODO: entities to manage multiple aplications!

  $form['oauth'] = array(
    '#type' => 'fieldset',
    '#title' => t('OAuth Settings'),
    //'#access' => module_exists('oauth_common'),
    '#description' => t('To enable OAuth based access for vimeo, you must <a href="@url">register your application</a> with Vimeo and add the provided keys here.', array('@url' => 'https://developer.vimeo.com/apps')),
  );
  
  $form['oauth']['callback_url'] = array(
    '#type' => 'item',
    '#title' => t('Callback URL'),
    '#markup' => url('vimeoapi/oauth', array('absolute' => TRUE)),
  );

  $form['oauth']['vimeoapi_application_id'] = array(
    '#type' => 'textfield',
    '#title' => 'Application ID',
    '#default_value' => variable_get('vimeoapi_application_id'),
  );
  
  $form['oauth']['vimeoapi_consumer_key'] = array(
    '#type' => 'textfield',
    '#title' => 'Client ID',
    '#default_value' => variable_get('vimeoapi_consumer_key'),
    '#description' => t('Also known as Consumer Key or API Key.'),
  );

  $form['oauth']['vimeoapi_consumer_secret'] = array(
    '#type' => 'textfield',
    '#title' => 'Client Secret',
    '#default_value' => variable_get('vimeoapi_consumer_secret'),
    '#description' => t('Also known as Consumer Secret or API Secret.'),
  );

  $form['oauth']['vimeoapi_access_token'] = array(
    '#type' => 'textfield',
    '#title' => 'Access Token',
    '#default_value' => variable_get('vimeoapi_access_token'),
  );

  return system_settings_form($form);
}

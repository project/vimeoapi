<?php

use Vimeo\Vimeo;

/**
 * Form to add an authenticated Vimeo account.
 */
function vimeoapi_oauth_account_form($form, &$form_state) {

  $form['scope'] = array(
    '#type'     => 'checkboxes',
    '#title'    => t('Scopes'),
    '#options'  => vimeoapi_scopes(),
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Go to Vimeo to add an authenticated account'),
  );

  return $form;
}

/**
 * Form validation for adding a new Vimeo account.
 */
function vimeoapi_oauth_account_form_validate($form, &$form_state) {
  $key    = variable_get('vimeoapi_consumer_key');
  $secret = variable_get('vimeoapi_consumer_secret');
  if (empty($key) || empty($secret)) {
    form_set_error('', t('Please configure your consumer key and secret key at ' .
      '<a href="!url">Vimeo settings</a>.', array( '!url' => url('admin/config/services/vimeoapi'),
    )));
  }
}

/**
 * Form submit handler for adding a Vimeo account.
 */
function vimeoapi_oauth_account_form_submit(&$form, &$form_state) {
  $key = variable_get('vimeoapi_consumer_key', '');
  $secret = variable_get('vimeoapi_consumer_secret', '');

  $vimeo = new Vimeo($key, $secret);

  $redirect_uri = url('vimeoapi/oauth', array('absolute' => TRUE));

  $scope = array();
  foreach ($form_state['values']['scope'] as $value) {
    if (!empty($value)) {
      $scope[] = $value;
    }
  }

  $_SESSION['vimeoapi_state'] = base64_encode(openssl_random_pseudo_bytes(30));
  $form_state['redirect'] =  $vimeo->buildAuthorizationEndpoint($redirect_uri, $scope, $_SESSION['vimeoapi_state']);
}

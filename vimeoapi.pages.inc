<?php

use Vimeo\Vimeo;

/**
 * Wrapper to call drupal_form_submit() which wasn't required in D6.
 */
function vimeoapi_oauth_callback() {

  if ($_SESSION['vimeoapi_state'] == $_GET['state'] && !empty($_GET['code'])) {
    // Deleting session variable!
    unset($_SESSION['vimeoapi_state']);

    $key    = variable_get('vimeoapi_consumer_key');
    $secret = variable_get('vimeoapi_consumer_secret');

    $vimeo = new Vimeo($key, $secret);
    $tokens = $vimeo->accessToken($_GET['code'], vimeoapi_redirect_url());

    if ($tokens['status'] == 200) {
      variable_set('vimeoapi_access_token', $tokens['body']['access_token']);
      drupal_set_message('Successful authentication');
    }
    else {
      drupal_set_message('Unsuccessful authentication', 'error');
    }

    drupal_goto('admin/config/services/vimeoapi');
	}
  else {
    drupal_set_message(t('The connection to Vimeo failed. Please try again.'), 'error');
    global $user;
    if ($user->uid) {
      // User is logged in, was attempting to OAuth a Vimeo account.
      drupal_goto('admin/config/services/vimeoapi/account/add');
    }
    else {
      // Anonymous user, redirect to front page.
      drupal_goto('<front>');
    }
  }
}
